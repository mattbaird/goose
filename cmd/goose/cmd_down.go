package main

import (
	"bitbucket.org/mattbaird/goose/lib/goose"
	"log"
)

var downCmd = &Command{
	Name:    "down",
	Usage:   "",
	Summary: "Roll back the version by 1",
	Help:    `down extended help here...`,
	Run:     downRun,
}

func downRun(cmd *Command, args ...string) {

	conf, err := dbConfFromFlags()
	if err != nil {
		log.Fatal(err)
	}
	if conf == nil {
		log.Fatal("dbconf parameter did not find a conf to load")
	}
	current, err := goose.GetDBVersion(conf)
	if err != nil {
		log.Fatal(err)
	}

	previous, err := goose.GetPreviousDBVersion(conf.MigrationsDir, current)
	if err != nil {
		if conf != nil {
			log.Fatalf("Configuration Directory:%s", conf.MigrationsDir)
		} else {
			log.Fatal("Configuration was nil/empty")
		}
		log.Fatal(err)
	}

	if err = goose.RunMigrations(conf, conf.MigrationsDir, previous, false); err != nil {
		log.Fatal(err)
	}
}
